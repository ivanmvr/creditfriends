
var user = window.location.pathname.split("/").slice(-1)[0];

// var socket = io();
//Envìa Msg de Nueva Solcitud al servidor
// function enviarSolicitud() {
//      socket.emit('enviarSolicitud', user);
//   };

  //Recibe Datos de Solicitudes Nuevas
  // var mensaje;
  // socket.on('solicitudEnviada', function(data) {
  //    mensaje = data.mensaje;
  //    //HTML
  //    alert(mensaje);
  // });

$(document).ready(function(){
  //Muestra form Nueva Solicitud
  $("#NuevaSolicitud").click(function(){
    $("#solicitudCont").show();
  });

});

var app = angular.module('dashboard', []);

app.controller('nombreCtrl', function($scope, $rootScope, $http) {
  	$scope.nombre = user;
  	//Verifica si ya existe el usuario
  	$http({
		method: 'POST',
    	url: '/userExists',
    	data: "nombre=" + user,
    	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  	}).then(function(response) {
  		var existe = response.data[1][0]["COUNT (nombre)"];
      if(existe)
      {
        $rootScope.$broadcast('existe');
      }else{
        //Inserta nuevo registro
        $http({
          method: 'POST',
          url: '/insertUser',
          data: "nombre=" + user,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
              console.log(response);
        }).catch(function(response) {
            console.error("Error " + response);
        })
      }
    }).catch(function(response) {
        console.error("Error " + response);
    })
});

app.controller('historyCtrl', function($scope, $rootScope, $http) {
  $scope.$on('existe', function(event) {
        $http({
          method: 'POST',
          url: '/getUserHistory',
          data: "nombre=" + user,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response) {
              console.log(response);
              //Muestra registros en la vista (checar si no tiene registros)

        }).catch(function(response) {
            console.error("Error " + response);
        })
  })
  $scope.enviarSolicitud = function() {
      //Insertar registro en BD
      var monto = parseInt($scope.solicmonto);
      var plazo = parseFloat($scope.solicpagos);
      var montoInteres = (monto * plazo) + monto;
      var estatus = "2";
      var usuario = user;

      $http({
        method: 'POST',
        url: '/sendReq',
        data: "monto=" + monto + "&montoInteres=" + montoInteres + "&Estatus=2" + "&user=" + user,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }).then(function(response) {
            console.log(response);
            $rootScope.$broadcast('obtenerSolic');
      }).catch(function(response) {
          console.error("Error " + response);
      })
    };
});

app.factory('socket', function($rootScope) {

    var socket = io.connect('localhost:8081');

    return {
        on: function(eventName, callback) {
            socket.on(eventName, function() {
                var args = arguments;
                //console.log(args);
                $rootScope.$apply(function() {


                    callback.apply(socket, args);
                });
            });
        },
        emit: function(eventName, data, callback) {
            socket.emit(eventName, data, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        }
    };


});


app.controller('requestsCtrl', function($scope, $rootScope, socket){
  $scope.$on('obtenerSolic', function(event) {
      //Envìa Msg de obtener Solicitudes al servidor
      socket.emit('obtenerSolicitudes');
      //Recibe Datos de Solicitudes Nuevas
      var solicitudes;
      socket.on('solicitudEnviada', function(data) {
         solicitudes = data.solicitudes;
      });
  })

});





