var connection = require('./connection');

connection.con.connect(function(err) {
  if (err) throw err;
  console.log("Conectado!");
  connection.con.query("CREATE DATABASE Concredito", function (err, result) {
    if (err) throw err;
    console.log("Base de datos creada!");
  });
});