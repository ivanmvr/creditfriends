var express = require('express');
var http = require('http');

var app = express();
var server_ = http.createServer(app);
var io = require('socket.io')(server_);

var bodyParser = require('body-parser');
var gh = require('./getHistory');
var ue = require('./userExists');
var iu = require('./insertUser');
var sr = require('./sendReq');
var gr = require('./getRequests');

var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.use(express.static('public'));

app.get('/index.html', function (req, res) {
   res.sendFile( __dirname + "/" + "index.html" );
})

app.get('/dashboard/:user', function (req, res) {
    res.sendFile( __dirname + "/" + "dashboard.html", function(){
   });
})

app.post('/getUserHistory', urlencodedParser, function (req, res) {
	gh.getCredHistory(req.body.nombre, res);
})

app.get('/redirecciona', function (req, res) {
   res.redirect('/dashboard/' + req.query.nombre);
})

app.post('/userExists', urlencodedParser, function (req, res) {
	ue.userExists(req.body.nombre, res);
})

app.post('/insertUser', urlencodedParser, function (req, res) {
	iu.insertUser(req.body.nombre, res);
})

app.post('/sendReq', urlencodedParser, function (req, res) {
  sr.sendReq(req.body.monto, req.body.montoInteres, req.body.Estatus, req.body.user, res);
})

io.on('connection', function(socket) {
   console.log('Usuario conectado');
   socket.on('obtenerSolicitudes', function() {
      //Envìa solicitud a tooos los demas usuarios
      var solicitudes = gr.getRequests();
      console.log(solicitudes);
      // socket.emit('solicitudEnviada', solicitudes);
   })
});

var server = server_.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("http://%s:%s", host, port)
})