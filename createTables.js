var connection = require('./connection');

connection.con.connect(function(err) {
  if (err) throw err;
  console.log("Conectado!");
  var sql1 = "USE Concredito;";
  var sql2 = "CREATE TABLE usuarios (id INT AUTO_INCREMENT PRIMARY KEY, nombre VARCHAR(255));";
  var sql3 = "CREATE TABLE solicitudes (id INT AUTO_INCREMENT PRIMARY KEY, idUsuario INT(4), monto INT(4), montoIntereses INT(4), votosSi INT(4), votosNo INT(4), estatus CHAR(1), FOREIGN KEY (idUsuario) REFERENCES Usuarios(id));";
  connection.con.query(sql1 + sql2 + sql3, function (err, result) {
    if (err) throw err;
    console.log("Tablas creadas!");
    });
});